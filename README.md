# 项目名称: yanfuti_icc2_block

## 重要声明
本Lab包含所有脚本不收取任何费用，同时也不承担任何技术支持的义务，但是接受合理的问题反馈并适时更新。<br>
本套脚本仅供学习交流，很多实际中可能遇到的问题细节和约束并未体现在本套脚本中，因此 **切不可用于实际项目的signoff，否则后果自负** 。<br>

问题反馈方法：
1) 在仓库下方的Issue部分直接评论，详细说明问题的现象和自己的发现<br>
2) 使用gitee邮箱9261063+yanfuti@user.noreply.gitee.com<br>
3) 加VX chen2mao2574免费入后端群讨论和反馈<br>

所有需要的EDA工具请支持正版。<br>

## 前言
本项目致力于免费分享基于ICC2的后端设计基本知识。鉴于绝大部分工艺库和设计信息都含有敏感的版权信息，能够找到比较合适的可公开资源并不容易。<br>
Synopsys的这套SAED工艺库是免费获取的对后端工具支持比较全面的工艺库之一，同时也有比较丰富的官方Lab资源大量使用此套库，因此十分有利于初学者学习。<br>
尽管官方Lab已经有比较多的素材，但是在没有详细文档指引的前提下对于初学者来说仍然不太友好。因此制作了这个Lab并配合一些简单的指引方便初学者学习。<br>
Lab包含使用ICC2实现后端设计的所需步骤：从floorplan到chipfinish，不包含后续所需要的各种验证步骤。<br>
本Lab的所有脚本全部是原创开发，不依赖现有的任何设计流程和官方Lab脚本，每条命令都是本人在考虑实际应用的前提前下选择使用的，几乎没有哪条命令是多余的，<br>
因此也强烈建议初学者仔细学习每一条命令的含义和作用。<br>

## Lab基本信息
基于Synopsys SAED32/28nm工艺库的ICC2模块设计Lab，设计基本信息如下所示：<br>
![设计信息](https://images.gitee.com/uploads/images/2021/0920/150733_8470d352_9261063.jpeg "info_small.jpg")<br>
本套Lab的主要输入数据（网表、SDC、scandef和library等）是从官方Lab中直接拷贝，其余脚本和命令都属原创。<br>

## Lab测试环境和工具版本
![环境和工具版本](https://images.gitee.com/uploads/images/2021/0920/150854_f983fabc_9261063.jpeg "tools.jpg")<br>

## Lab结构介绍
Lab的输入文件主要分为三个部分：library, scripts和data，其详细内如如下：<br>
library: 包含所需要的库（ndm, lef等）、tech file(icc2, starrc, icv等)<br>
scripts: 包含每个步骤需要的所有脚本<br>
data   : 包含所需要的输入文件(netlist, sdc, scandef, upf等)<br>
输出文件将会在后续介绍。<br>

## 快速上手步骤
1) 打开run_all.csh (vim/gvim/emac/notepad++等任意文本编辑器)<br>
2) 修改以下部分<br>
   a) 将以下粗体部分替换成icc2的安装目录，指定到bin一级目录（必须）<br>
      set path = ( $path  **/your/icc2/path/to/bin**  )<br>
   b) 将以下粗体部分替换成synopsys的license server或者license文件路径（必须）<br>
      setenv SNPSLMD_LICENSE_FILE " **/your/license/server/or/file** "<br>
   c) 如果系统安装了LSF，修改以下所有行的粗体部分，替换成想要使用的queue（可选）<br>
      bsub -Ip -q  **normal ** -n 2 icc2_shell -f scripts/* ...<br>
   d) 如果系统并未安装LSF，则注释掉和上述行类似的所有行，同时将以下行首的注释取消（可选）<br>
      #icc2_shell -f scripts/*<br>
3) 运行run_all.csh<br>
   ./run_all.csh<br>
4) 查看结果<br>
   a) 所有log会产生在./logs/目录下，需要查看每个步骤是否有Error<br>
      routeopt的starrc in design默认会有Error，具体后续介绍<br>
   b) 报告(reports)将会产生在./reports/*目录下，每个step分开产生<br>
   c) 数据将会产生在./data目录下，每个step的ndm library放在./data/nlib下<br>

## 主要脚本功能介绍
### scripts/00_common_initial_settings.tcl
通用设置，例如：设计名称、初始数据、各类目录、库和tech文件、scenario设置、filler/decap/endcap等physical only cell类型等<br>

### scripts/01_icc2_import_design.tcl
读入初始网表和UPF，其中UPF采用Golden UPF Flow。<br>
关于Golden UPF Flow的概念如过不清楚请先查阅相关资料。<br>

### scripts/02_create_floorplan.tcl
生成floorplan信息：core/die形状，power domain，macro和pin的摆放，keepout margin，插入boundary cell和tap cell等<br>
Floorplan形状默认是方形，可以根据实际需要练习多边形的floorplan，脚本中有简单的指引，也可以在icc2中man initialize_floorplan命令查看详细用法。<br>
Port位置默认放在左右两边，建议练习更换摆放边和金属层。<br>
Voltage Area的大小，位置和hard macro的位置建议作为重点练习，尝试更改macro的位置迭代place的结果。<br>

### scripts/03_power_routing.tcl
电源网络规划和绕线：standard cell区域和sram，不同voltage area内的电源生成等<br>
电源绕线在ICC2中术语比较复杂的部分，由于要支持非常复杂的pattern，因此相关命令的用法也比较复杂。<br>
本Lab使用的工艺比较成熟，因此没有使用非常复杂的pattern，但是每条命令仍然比较复杂，建议配合man信息逐条学习<br>

### scripts/04_placeopt.tcl
Pre-CTS的时序、面积和Congestion相关设置和优化<br>

### scripts/05_clock.tcl
时钟树综合，默认不开启CCD功能<br>

### scripts/06_clockopt.tcl
Post-CTS的时序、面积和Congestion相关设置和优化<br>

### scripts/07_route.tcl
绕线以及相关设置<br>

### scripts/08_routeopt.tcl
绕线后的时序、面积和Congestion相关设置和优化。默认开启starrc-in-design功能。<br>
该功能需要进一步相关脚本，具体步骤请参考starrc_in_design.config.txt和starrc_in_design.corners.txt部分。<br>

### scripts/09_chipfinish.tcl
PnR结束前的最后一步，插入filler并生成后续验证需要的数据：网表、GDS/OASIS、DEF等。<br>

### starrc_in_design.config.txt
starrc-in-design功能简介：ICC2中的原生寄生抽取引擎与Signoff级别的工具StarRC并不相同，从而会导致PnR阶段的寄生参数和Signoff存在误差。<br>
为了解决此问题，Synopsys公司开发了Fusion技术，其中之一就是在PnR工具ICC2中调用StarRC工具来抽取寄生参数。该功能的配置需要以下脚本的设置：<br>
其内容包含以下四行：<br>
SIGNOFF_IMAGE: /data/eda/synopsys/starrc/R-2020.09-SP3/bin/StarXtract<br>
MAPPING_FILE: ./library/tech//starrc/saed32nm_tf_itf_tluplus.starrc_in_design.map<br>
CORNER_GRD_FILE:./scripts/starrc_in_design.corners.txt<br>
COMMAND_FILE:./scripts/starrc_in_design.cmd<br>
其中SIGNOFF_IMAGE需要指定系统安装的starrc路径，指向StarXtract可执行文件，需要绝对路径；<br>
MAPPING_FILE指定tf和nxtgrd的layer map文件，默认已经写好，建议转换成绝对路径；<br>
CORNER_GRD_FILE指定starrc各个corner所需要的tech文件，后续详细介绍，建议转换成绝对路径；<br>
COMMAND_FILE指定starrc的命令文件，默认已经写好，建议转换成绝对路径；<br>

### scripts/starrc_in_design.corners.txt
包含以下三行，用于定义不同corner对应使用的tech文件。默认已经写好，但是建议将tech文件转换成绝对路径；<br>
ss0p75v125c_cmax ./library/tech/starrc/saed32nm_1p9m_Cmax.nxtgrd<br>
ff0p95vm40c_cmin ./library/tech/starrc/saed32nm_1p9m_Cmin.nxtgrd<br>
ff0p95v125c_cmin ./library/tech/starrc/saed32nm_1p9m_Cmin.nxtgrd<br>

### scripts/scenarios_setup.tcl
定义每个scenario的约束文件，包含mode, corner和scenario的创建以及每个scenario的详细约束。<br>
此文件中定义了所有步骤中需要用到的scenario并做出了必要的判断，因此所有步骤通用此文件。<br>

### scripts/initialization_settings.tcl<br>
初始化ICC2工具，定义如时间单位、site属性、默认绕线方向和绕线层等信息<br>

## 结语
本Lab仅可用于了解和学习后端流程和工具基本用法，实际项目中更加详细的约束如DRV，uncertainty，OCV和signoff PVT条件等都没有考虑或不准确。<br>
由于Synopsys ICC2在2019后有较大更新，因此EDA工具版本建议使用较新的2019以后的版本，新版本的优化结果更能体现真正的问题所在。<br>
如果对本Lab的流程和脚本有任何意见和建议，欢迎反馈。
