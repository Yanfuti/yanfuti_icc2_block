foreach layer [get_object_name [get_layers M*]] {
    set minwidth [get_att [get_layers ${layer}] min_width]
    set minspacing [get_att [get_layers ${layer}] min_spacing]
    set minwidthx2 [expr $minwidth * 2]
    set minspacingx2 [expr $minspacing * 2]
    #echo "$layer : $minwidthx2 : $minspacingx2"
    #echo "$layer $minwidthx2"
    echo "$layer $minspacingx2"
}

