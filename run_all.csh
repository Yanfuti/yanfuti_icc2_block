#!/usr/bin/tcsh -f

### start icc2
set path = ( $path /your/icc2/path/to/bin )
setenv SNPSLMD_LICENSE_FILE "/your/license/server/or/file"
mkdir -p logs

### if you have lsf installed in your system, please config below to start runs
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/01_icc2_import_design.tcl |& tee -i logs/icc2.01_floorplan.log
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/02_create_floorplan.tcl |& tee -i logs/icc2.02_create_floorplan.log
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/03_power_routing.tcl |& tee -i logs/icc2.03_power_routing.log
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/04_placeopt.tcl |& tee -i logs/icc2.04_placeopt.log
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/05_clock.tcl |& tee -i logs/icc2.05_clock.log
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/06_clockopt.tcl |& tee -i logs/icc2.06_clockopt.log
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/07_route.tcl |& tee -i logs/icc2.07_route.log
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/08_routeopt.tcl |& tee -i logs/icc2.08_routeopt.log
#bsub -Ip -q normal -n 2 icc2_shell -f scripts/09_chipfinish.tcl |& tee -i logs/icc2.09_chipfinish.log

### if no lsf installed, just use below command to start runs
#icc2_shell -f scripts/01_icc2_import_design.tcl |& tee -i logs/icc2.01_floorplan.log
#icc2_shell -f scripts/02_create_floorplan.tcl |& tee -i logs/icc2.02_create_floorplan.log
#icc2_shell -f scripts/03_power_routing.tcl |& tee -i logs/icc2.03_power_routing.log
#icc2_shell -f scripts/04_placeopt.tcl |& tee -i logs/icc2.04_placeopt.log
#icc2_shell -f scripts/05_clock.tcl |& tee -i logs/icc2.05_clock.log
#icc2_shell -f scripts/06_clockopt.tcl |& tee -i logs/icc2.06_clockopt.log
#icc2_shell -f scripts/07_route.tcl |& tee -i logs/icc2.07_route.log
#icc2_shell -f scripts/08_routeopt.tcl |& tee -i logs/icc2.08_routeopt.log
#icc2_shell -f scripts/09_chipfinish.tcl |& tee -i logs/icc2.09_chipfinish.log


