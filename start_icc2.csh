#!/usr/bin/tcsh -f

### start icc2
set path = ( $path /your/icc2/path/to/bin )
setenv SNPSLMD_LICENSE_FILE "/your/license/server/or/file"

### if you have lsf installed in your system, please config below to start icc2
set current_time = `date +"%Y%m%d_%H%M%S"`
set logfile = "logs/icc2.interactive.${current_time}.log"
mkdir -p logs
bsub -Ip -q normal -n 2 icc2_shell |& tee -i $logfile

### if no lsf installed, just use below command to start icc2
#icc2_shell |& tee -i $logfile
